import org.apache.commons.lang3.math.IEEE754rUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumTest {

	public static void main(String[] args) {
		// Running Chrome Browser
		WebDriver driver= null;
		WebDriverManager.chromedriver().setup();   //if you want to run latest version of browser
//		WebDriverManager.chromedriver().version("2.36").setup();  //if you want to run specific version of browser
		 driver = new ChromeDriver();
		 openBrowser(driver);
		
		//Running Firefox browser
		 WebDriverManager.firefoxdriver().setup();
		 driver = new FirefoxDriver();
		 openBrowser(driver);
		
		//Running IE browser
		WebDriverManager.iedriver().setup();
		 driver= new InternetExplorerDriver();
		 openBrowser(driver);
		
	}

	/**
	 * @param driver
	 */
	private static void openBrowser(WebDriver driver) {
		driver.get("https://google.com");		
		//driver.findElement(By.className("gLFyf gsfi")).sendKeys("sanjib");
		driver.findElement(By.xpath("//input[@type='text'][@name='q']")).sendKeys("hello");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/form[1]/div[2]/div[1]/div[2]/div[2]/ul[1]/li[1]/div[1]/div[1]/div[1]/span[1]	")).click();
		//driver.findElement(By.xpath("//input[@type='text'][@name='q']")).click();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.close();
	}

}
